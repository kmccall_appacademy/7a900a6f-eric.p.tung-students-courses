class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @last_name = last_name
    @first_name = first_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    if @courses.include?(new_course)
      # raise Exception.new("Course already exists!")
    else
      if self.has_conflict?(new_course)
        raise Exception.new("Course conflicts with existing schedule")
      else
        @courses << new_course
        new_course.students << self
      end
    end
  end

  def has_conflict?(new_course)
    @courses.any? { |course| course.conflicts_with?(new_course) }
  end

  def course_load
    total_credits=Hash.new(0)
    @courses.each { |course| total_credits[course.department]+=course.credits }
    total_credits
  end
end
